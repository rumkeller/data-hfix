module Data.Fix
  ( Fix(..)
  , cata
  , ana
  , hylo
  ) where

data Fix (f :: * -> *) = Fix { unFix :: f (Fix f) }

cata :: Functor f => (f a -> a) -> (Fix f -> a)
cata f = f . fmap (cata f) . unFix

ana :: Functor f => (a -> f a) -> (a -> Fix f)
ana f = Fix . fmap (ana f) . f

hylo :: Functor f => (f b -> b) -> (a -> f a) -> (a -> b)
hylo phi psi = cata phi . ana psi
